# Portfolio API 

This project is an API to server Portfolio information from a no-sql Database.

It was built mainly on top the following tech stacks

* Typescript as a programming language. <https://www.typescriptlang.org/>
* NestJS as the Web Framework for Node.js <https://nestjs.com/> 
* DynamoDB as Database <https://aws.amazon.com/es/dynamodb/>
* Dynamoose as Modeling Tool <https://github.com/dynamoose/dynamoose>
* Serverless as a execution model 
* AWS-Lambdas as Cloud platform to host the serverless solution. 

## Other Libraries 

* oauth-1.0a to consume the Twitter APÏ via oauth
* serverless-offline for local deployment and testing
* dotenv for easy enviroment configuration
* nestjx/automapper <https://github.com/nestjsx/automapper>
* class-validator <https://github.com/typestack/class-validator>

## Tests
* Cypress

There is a test available for this API in this same project but in another repository. <https://bitbucket.org/PedroJoseDA91/portfoliotest/src/master/>
Ro be able to test it, you only need to clone that repo and run 
```
$ npm install 
$ npx cypress open 
```
and click on the test called **TEST01_API.spec.js**

This project makes extensive use of SOLID Principles and Clean Architecture. 

To be able to run this project, you must first clone it, then generate a .env file following the example and then run the following commands. 
Node.js >= 12.15  is expected. 
```
$ npm install 
$ npm run build 
$ sls offline start
```

Theres a live demo of this API running in 
<https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/>

The root of this url is currently serving  a client page that renders the output of 
```GET https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/portfolio/pdeaguas```

This API includes the following methods 
```GET https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/portfolio/{Id}``` To query one specific portfolio.
```GET https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/portfolio``` To query all current portfolios.
```POST https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/portfolio``` To create a new portfolio.
```PUT https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/portfolio/{Id}``` To update one specific portfolio.
```DELETE https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/portfolio/{Id}``` To delete one specific portfolio.