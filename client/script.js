import constants from "./consts.js"
var app = new Vue({
    el: "#app",
    data: {
        description: "",
        id: "",
        email: "",
        name: "",
        title: "",
        isLoading: true,
        twitterFeed: [],
        imageUrl: "https://via.placeholder.com/150x150"
    },
    beforeMount() {
        axios
            .get(
                `${constants.portfolioApi}/pdeaguas`
            )
            .then((response) => {
                this.description = response.data.description;
                this.id = response.data.id;
                this.email = response.data.email;
                this.name = response.data.name;
                this.title = response.data.title;
                this.imageUrl = response.data.imageUrl;
                this.isLoading = false;
            });


        axios
            .get(
                `${constants.twitterFeedApi}`
            )
            .then((response) => {
                this.twitterFeed = response.data.slice(0, 5) // only 5
            })
    },
});
