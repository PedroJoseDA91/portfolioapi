const constants = {
    portfolioApi: "https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/portfolio",
    portfolioTestApi: "https://localhost:3000/portfolio",
    twitterFeedApi: "https://zdzhq3jr99.execute-api.sa-east-1.amazonaws.com/zem/twitter-feed",
    twitterFeedTestApi: "http://localhost:3000/twitter-feed"
};
export default constants;