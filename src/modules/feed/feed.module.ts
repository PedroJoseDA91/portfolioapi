import { HttpModule, HttpService, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { FEED_FETCHER } from './domain/feedfetcher.interface';
import { FeedController } from './feed.controller';
import { FeedFetcherService } from './feedfetcher.service';

@Module({
  imports: [HttpModule],
  controllers: [FeedController],
  providers: [
    {
      useClass: FeedFetcherService,
      provide: FEED_FETCHER
    },
    ConfigService]
})
export class FeedModule { }
