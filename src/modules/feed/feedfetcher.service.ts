import { HttpService, Injectable } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ConfigurationEnum } from 'src/config/config.keys';
import { FeedFetcher } from './domain/feedfetcher.interface';

const OAuth = require('oauth-1.0a')
const crypto = require('crypto')

@Injectable()
export class FeedFetcherService implements FeedFetcher {
    constructor(
        private _httpService: HttpService,
        private config: ConfigService
    ) { }
    async getFeed(): Promise<string[]> {
        return (await this.obtainTwitterFeed()).map(element => {
            return element.text;
        });
    }

    private async obtainTwitterFeed(): Promise<any[]> {
        const oauth = OAuth({
            consumer: { key: this.config.get(ConfigurationEnum.CONSUMER_KEY), secret: this.config.get(ConfigurationEnum.CONSUMER_SECRET) },
            signature_method: 'HMAC-SHA1',
            hash_function(base_string, key) {
                return crypto
                    .createHmac('sha1', key)
                    .update(base_string)
                    .digest('base64');
            },
        })

        const requestData = {
            url: this.config.get(ConfigurationEnum.TWITTER_API),
            method: 'GET'
        }

        const token = {
            key: this.config.get(ConfigurationEnum.TOKEN_KEY),
            secret: this.config.get(ConfigurationEnum.TOKEN_SECRET),
        }

        const res = await this._httpService.get(requestData.url, {
            headers: oauth.toHeader(oauth.authorize(requestData, token)),
            withCredentials: true
        }).toPromise();
        return res.data;
    }
}
