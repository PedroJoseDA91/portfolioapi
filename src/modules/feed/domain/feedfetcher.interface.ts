export const FEED_FETCHER = "FEED_FETCHER";
export interface FeedFetcher {
    getFeed(): Promise<string[]>
}