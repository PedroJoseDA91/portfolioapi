import { Controller, Get, HttpService, Inject } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { FeedFetcher, FEED_FETCHER } from './domain/feedfetcher.interface';
import { FeedFetcherService } from './feedfetcher.service';
@Controller('twitter-feed')
export class FeedController {
    constructor(@Inject("FEED_FETCHER") private _feedFetcherService: FeedFetcher) { }
    @Get()
    async get() {
        return this._feedFetcherService.getFeed();
    }
}
