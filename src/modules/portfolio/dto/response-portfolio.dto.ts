export class ResponsePortfolio {
    name?: string;
    title?: string;
    description?: string;
    imageUrl?: string;
    email?: string;
}