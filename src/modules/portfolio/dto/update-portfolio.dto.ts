import { Portfolio } from "../domain/portfolio.interface";
import { IsEmail, IsNotEmpty, IsOptional } from 'class-validator';

export class UpdatePortfolio implements Portfolio {
    name?: string;
    title?: string;
    description?: string;
    imageUrl?: string;
    id: string;
    @IsOptional()
    @IsEmail()
    email?: string;
}