import { Portfolio } from "../domain/portfolio.interface";
import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreatePortfolio implements Portfolio {
    name?: string;
    title?: string;
    description?: string;
    imageUrl?: string;
    @IsNotEmpty()
    id: string;
    @IsEmail()
    email: string;
}