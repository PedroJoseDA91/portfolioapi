import { HttpException, HttpStatus, Inject, Injectable, NotFoundException } from '@nestjs/common';
import { PortfolioGetter } from './domain/portfolioGetter.interface';
import { PortfolioDomainRepository } from './domain/portfolioDomainRepository.interface';
import { PortfolioRepository } from './portfolio.repository';
import { Portfolio } from './domain/portfolio.interface';
import { ErrorFactory } from 'src/shared/ExceptionFactory';
import { ERROR_TYPE } from 'src/shared/domain/ErrorType.Enum';

@Injectable()
export class PortfolioGetterService implements PortfolioGetter {
    constructor(
        @Inject("PORTFOLIO_REPOSITORY")
        private _portfolioRepository: PortfolioDomainRepository,
        private _errorFactory: ErrorFactory
    ) { }

    async getAll(): Promise<Portfolio[]> {
        const portfolios: Promise<Portfolio[]> = this._portfolioRepository.getAll();
        if ((await portfolios).length == 0) {
            const exception = this._errorFactory.getError(ERROR_TYPE.PORTFOLIONOTFOUND);
            throw new HttpException(exception.toObject(), exception.statusCode);
        }
        return portfolios;
    }

    async getById(id: string): Promise<Portfolio> {
        const portfolio: Promise<Portfolio> = this._portfolioRepository.get(id);
        if ((await portfolio) === undefined) {
            const exception = this._errorFactory.getError(ERROR_TYPE.PORTFOLIONOTFOUND);
            throw new HttpException(exception.toObject(), exception.statusCode);
        }
        return portfolio;
    }

}
