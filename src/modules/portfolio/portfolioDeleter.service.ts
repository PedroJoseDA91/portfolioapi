import { HttpException, Inject, Injectable } from '@nestjs/common';
import { PortfolioDeleter } from './domain/portfolioDeleter.interface';
import { PortfolioDomainRepository } from './domain/portfolioDomainRepository.interface';
import { Portfolio } from './domain/portfolio.interface';
import { PortfolioRepository } from './portfolio.repository';
import { ErrorFactory } from 'src/shared/ExceptionFactory';
import { ERROR_TYPE } from 'src/shared/domain/ErrorType.Enum';

@Injectable()
export class PortfolioDeleterService implements PortfolioDeleter {
    constructor(
        @Inject("PORTFOLIO_REPOSITORY")
        private _portfolioRepository: PortfolioDomainRepository,
        private _errorFactory: ErrorFactory
    ) { }

    async delete(id: string): Promise<void> {
        const portfolio = this._portfolioRepository.get(id);
        if ((await portfolio) === 0) {
            const exception = this._errorFactory.getError(ERROR_TYPE.PORTFOLIONOTFOUND);
            throw new HttpException(exception.toObject(), exception.statusCode);
        }
        this._portfolioRepository.delete(id);
    }

}
