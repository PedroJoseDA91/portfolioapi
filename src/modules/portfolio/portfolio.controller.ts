import {
    Body,
    Controller, Delete, Get, Inject, Param, Post, Put, Res, ValidationPipe
} from '@nestjs/common';
import { InjectMapper, AutoMapper } from 'nestjsx-automapper';

import { PortfolioCreator } from './domain/portfolioCreator.interface';
import { PortfolioDeleter } from './domain/portfolioDeleter.interface';
import { PortfolioGetter } from './domain/portfolioGetter.interface';
import { PortfolioUpdater } from './domain/portfolioUpdater.interface';
import { Portfolio } from './domain/portfolio.interface';
import { CreatePortfolio } from './dto/create-portfolio.dto';
import { UpdatePortfolio } from './dto/update-portfolio.dto';
import { ResponsePortfolio } from './dto/response-portfolio.dto';
import { DomainPortfolio } from './domain/domain-portfolio';
import './response-portfolio.profile';
@Controller('portfolio')
export class PortfolioController {
    constructor(
        @Inject("PORTFOLIO_GETTER") private _portfolioGetterService: PortfolioGetter,
        @Inject("PORTFOLIO_CREATOR") private _portfolioCreatorService: PortfolioCreator,
        @Inject("PORTFOLIO_DELETER") private _portfolioDeleterService: PortfolioDeleter,
        @Inject("PORTFOLIO_UPDATER") private _portfolioUpdaterService: PortfolioUpdater,
        @InjectMapper() private readonly mapper: AutoMapper
    ) { }

    @Get()
    async getAll(): Promise<any> {
        const portfolios: Promise<Portfolio[]> = await this._portfolioGetterService.getAll().catch(err => {
            return err;
        });

        return (await portfolios).map(portfolio => this.mapper.map(portfolio, ResponsePortfolio, DomainPortfolio));
    }

    @Get(":id")
    async get(@Param('id') id: string, @Res() res): Promise<void | Portfolio> {
        var portfolio = await this._portfolioGetterService.getById(id).catch(err => {
            res.status(err.response.statusCode);
            res.send(err.response);
        });
        if (res.headersSent) return;
        res.send(this.mapper.map(await portfolio, ResponsePortfolio, DomainPortfolio));
    }

    @Post()
    async create(@Body(new ValidationPipe()) portfolio: CreatePortfolio, @Res() res): Promise<void | Portfolio> {
        var newPortfolio = await this._portfolioCreatorService.create(portfolio).catch(err => {
            res.status(err.response.statusCode);
            res.send(err.response);
        });
        if (res.headersSent) return;
        res.send(this.mapper.map(await newPortfolio, ResponsePortfolio, DomainPortfolio));
    }

    @Put(":id")
    async update(@Param('id') _id, @Body(new ValidationPipe()) portfolio: UpdatePortfolio, @Res() res): Promise<void | Portfolio> {
        var updatedPortfolio = await this._portfolioUpdaterService.update(_id, portfolio).catch(err => {
            res.status(err.response.statusCode);
            res.send(err.response);
        });
        if (res.headersSent) return;
        res.send(this.mapper.map(await updatedPortfolio, ResponsePortfolio, DomainPortfolio));
    }

    @Delete(":id")
    async delete(@Param('id') id: string, @Res() res): Promise<void> {
        await this._portfolioDeleterService.delete(id).catch(err => {
            res.status(err.response.statusCode);
            res.send(err.response);
        });
        if (res.headersSent) return;
        res.send();
    }
}
