import { HttpException, Inject, Injectable } from '@nestjs/common';
import { PortfolioCreator } from './domain/portfolioCreator.interface';
import { PortfolioDomainRepository, PORTFOLIO_REPOSITORY } from './domain/portfolioDomainRepository.interface';
import { Portfolio } from './domain/portfolio.interface';
import { PortfolioRepository } from './portfolio.repository';
import { ErrorFactory } from 'src/shared/ExceptionFactory';
import { ERROR_TYPE } from 'src/shared/domain/ErrorType.Enum';

@Injectable()
export class PortfolioCreatorService implements PortfolioCreator {
    constructor(
        @Inject("PORTFOLIO_REPOSITORY")
        private _portfolioRepository: PortfolioDomainRepository,
        private _errorFactory: ErrorFactory
    ) { }

    async create(portfolio: Portfolio): Promise<Portfolio> {
        if (portfolio.id === undefined) {
            const exception = this._errorFactory.getError(ERROR_TYPE.IDISMISSING);
            throw new HttpException(exception.toObject(), exception.statusCode);
        }
        const tryPortfolio = this._portfolioRepository.get(portfolio.id);
        if ((await tryPortfolio) !== undefined) {
            const exception = this._errorFactory.getError(ERROR_TYPE.PORTFOLIOALREADYEXISTS);
            throw new HttpException(exception.toObject(), exception.statusCode);
        }
        return this._portfolioRepository.create(portfolio);
    }


}
