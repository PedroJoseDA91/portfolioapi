import { HttpException, Inject, Injectable } from '@nestjs/common';
import { PortfolioDomainRepository } from './domain/portfolioDomainRepository.interface';
import { PortfolioUpdater } from './domain/portfolioUpdater.interface';
import { Portfolio } from './domain/portfolio.interface';
import { PortfolioRepository } from './portfolio.repository';
import { ErrorFactory } from 'src/shared/ExceptionFactory';
import { ERROR_TYPE } from 'src/shared/domain/ErrorType.Enum';

@Injectable()
export class PortfolioUpdaterService implements PortfolioUpdater {
    constructor(
        @Inject("PORTFOLIO_REPOSITORY")
        private _portfolioRepository: PortfolioDomainRepository,
        private _errorFactory: ErrorFactory
    ) { }

    async update(id: string, portfolio: Portfolio): Promise<Portfolio> {
        const tryPortfolio = this._portfolioRepository.get(id);
        if ((await tryPortfolio) === undefined) {
            const exception = this._errorFactory.getError(ERROR_TYPE.PORTFOLIONOTFOUND);
            throw new HttpException(exception.toObject(), exception.statusCode);
        }
        portfolio.id = id;
        return this._portfolioRepository.update(portfolio);
    }

}
