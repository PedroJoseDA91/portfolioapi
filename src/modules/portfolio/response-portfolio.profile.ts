import { Profile, ProfileBase, AutoMapper, mapFrom } from 'nestjsx-automapper';
import { DomainPortfolio } from './domain/domain-portfolio';
import { ResponsePortfolio } from './dto/response-portfolio.dto';


@Profile()
export class ResponsePortfolioProfile extends ProfileBase {
    constructor(mapper: AutoMapper) {
        super();
        mapper
            .createMap(DomainPortfolio, ResponsePortfolio)
            .forMember(
                d => d.description,
                mapFrom(s => s?.description),
            )
            .forMember(
                d => d.email,
                mapFrom(s => s.email),
            )
            .forMember(
                d => d.imageUrl,
                mapFrom(s => s.imageUrl),
            )
            .forMember(
                d => d.name,
                mapFrom(s => s.name),
            )
            .forMember(
                d => d.title,
                mapFrom(s => s.title),
            );
    }
}
