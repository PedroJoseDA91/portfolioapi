import { Schema } from 'dynamoose'

export const PortfolioSchema = new Schema(
    {
        id: {
            type: String,
            hashKey: true,
        },
        name: {
            type: String
        },
        description: {
            type: String
        },
        imageUrl: {
            type: String
        },
        email: {
            type: String
        },
        userName: {
            type: String
        },
        title: {
            type: String
        }
    }
)

