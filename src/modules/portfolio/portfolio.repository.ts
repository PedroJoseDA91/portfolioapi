import { Injectable, Param } from "@nestjs/common";
import { InjectModel, Model } from "nestjs-dynamoose";
import { PortfolioDomainRepository } from "./domain/portfolioDomainRepository.interface";
import { Portfolio, PortfolioKey } from "./domain/portfolio.interface";

@Injectable()
export class PortfolioRepository implements PortfolioDomainRepository {
    constructor(
        @InjectModel('Portfolio')
        private _portfolioModel: Model<Portfolio, PortfolioKey>
    ) { }

    async getAll(): Promise<any> {
        var a = this._portfolioModel.scan().exec();
        return a;
    }

    async get(_id: string): Promise<any> {
        var a = this._portfolioModel.get({ id: _id });
        return a;
    }

    async create(portfolio: Portfolio): Promise<any> {
        var a = this._portfolioModel.create(portfolio);
        return a;
    }

    async update(portfolio: Portfolio): Promise<any> {
        var a = this._portfolioModel.update(portfolio);
        return a;
    }

    async delete(id: string): Promise<any> {
        var a = this._portfolioModel.delete({ id: id });
        return a;
    }
}