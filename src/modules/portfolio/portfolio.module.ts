import { Module } from '@nestjs/common';
import { DynamooseModule } from 'nestjs-dynamoose';
import { PortfolioController } from './portfolio.controller';
import { PortfolioRepository } from './portfolio.repository';
import { PortfolioSchema } from './portfolio.schema';
import { PortfolioCreatorService } from './portfolioCreator.service';
import { PortfolioDeleterService } from './portfolioDeleter.service';
import { PortfolioGetterService } from './portfolioGetter.service';
import { PortfolioUpdaterService } from './portfolioUpdater.service';
import { PORTFOLIO_GETTER } from './domain/portfolioGetter.interface';
import { PORTFOLIO_CREATOR } from './domain/portfolioCreator.interface';
import { PORTFOLIO_UPDATER } from './domain/portfolioUpdater.interface';
import { PORTFOLIO_DELETER } from './domain/portfolioDeleter.interface';
import { PORTFOLIO_REPOSITORY } from './domain/portfolioDomainRepository.interface';
import { ErrorFactory } from 'src/shared/ExceptionFactory';
@Module({
  imports: [
    DynamooseModule.forFeature([{ name: 'Portfolio', schema: PortfolioSchema }]),
  ],
  controllers: [PortfolioController],
  providers: [{
    useClass: PortfolioCreatorService,
    provide: PORTFOLIO_CREATOR
  }, {
    useClass: PortfolioUpdaterService,
    provide: PORTFOLIO_UPDATER
  }, {
    useClass: PortfolioDeleterService,
    provide: PORTFOLIO_DELETER
  },
  {
    useClass: PortfolioGetterService,
    provide: PORTFOLIO_GETTER
  }, {
    useClass: PortfolioRepository,
    provide: PORTFOLIO_REPOSITORY
  }, ErrorFactory]

})
export class PortfolioModule { }
