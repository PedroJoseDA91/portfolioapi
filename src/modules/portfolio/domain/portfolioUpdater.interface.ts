import { Portfolio } from "./portfolio.interface";

export const PORTFOLIO_UPDATER = "PORTFOLIO_UPDATER";
export interface PortfolioUpdater {
    update(id: string, portfolio: Portfolio): Promise<Portfolio>;
}