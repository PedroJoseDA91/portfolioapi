import { Portfolio } from "./portfolio.interface";

export const PORTFOLIO_GETTER = "PORTFOLIO_GETTER";
export interface PortfolioGetter {
    getById(id: string): Promise<Portfolio>;
    getAll(): Promise<Portfolio[]>;
}