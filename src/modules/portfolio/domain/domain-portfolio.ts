import { Portfolio } from "./portfolio.interface";

export class DomainPortfolio implements Portfolio {
    name?: string;
    title?: string;
    description?: string;
    imageUrl?: string;
    email: string;
    id: string;

}