import { Portfolio } from "./portfolio.interface";

export const PORTFOLIO_REPOSITORY = "PORTFOLIO_REPOSITORY"
export interface PortfolioDomainRepository {
    getAll();
    get(_id: string);
    create(portfolio: Portfolio);
    update(portfolio: Portfolio);
    delete(id: string);
}