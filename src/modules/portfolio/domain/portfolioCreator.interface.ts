import { Portfolio } from "./portfolio.interface";

export const PORTFOLIO_CREATOR = "PORTFOLIO_CREATOR";
export interface PortfolioCreator {
    create(portfolio: Portfolio): Promise<Portfolio>;
}