export interface PortfolioKey {
    id: string;
}

export interface Portfolio extends PortfolioKey {
    name?: string;
    title?: string;
    description?: string;
    imageUrl?: string;
    email?: string;
}