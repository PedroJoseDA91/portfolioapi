export const PORTFOLIO_DELETER = "PORTFOLIO_DELETER";
export interface PortfolioDeleter {
    delete(id: string): Promise<void>;
}