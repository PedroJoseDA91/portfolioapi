import { Module } from '@nestjs/common';
import { DynamooseModule } from 'nestjs-dynamoose';

import { PortfolioModule } from './modules/portfolio/portfolio.module';
import { FeedModule } from './modules/feed/feed.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './config/configuration';
import { ConfigurationEnum } from './config/config.keys';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AutomapperModule } from 'nestjsx-automapper';

@Module({
  imports: [
    AutomapperModule.withMapper(),
    ConfigModule.forRoot({
      load: [configuration]
    }),
    DynamooseModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      async useFactory(config: ConfigService) {
        return {
          aws: {
            accessKeyId: config.get(ConfigurationEnum.AWS_ACCESS_KEY_ID_USER),
            secretAccessKey: config.get(ConfigurationEnum.AWS_SECRET_ACCESS_KEY_USER),
            region: config.get(ConfigurationEnum.AWS_REGION_USER)
          }
        }
      },
    }),
    FeedModule,
    PortfolioModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client'),
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
