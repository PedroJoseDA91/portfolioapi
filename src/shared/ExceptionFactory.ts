import { Injectable } from "@nestjs/common";
import GettableError from "./domain/GettableError";
import { ERROR_TYPE } from "./domain/ErrorType.Enum";
import { IdIsMissing, InvalidFields, PortfolioAlreadyExists } from "./domain/PortfolioCreateErrors";
import PortfolioNotFound from "./domain/PortfolioGetErrors";

@Injectable()
export class ErrorFactory {
    getError(type: string): GettableError {
        switch (type) {
            case (ERROR_TYPE.PORTFOLIOALREADYEXISTS):
                return new PortfolioAlreadyExists()

            case (ERROR_TYPE.IDISMISSING):
                return new IdIsMissing()

            case (ERROR_TYPE.PORTFOLIONOTFOUND):
                return new PortfolioNotFound()

            case (ERROR_TYPE.INVALIDFIELDS):
                return new InvalidFields()
        }
    }
}