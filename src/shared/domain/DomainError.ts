export default interface DomainError {
    message: string;
    statusCode?: number;

}

