import { HttpStatus } from "@nestjs/common/enums/http-status.enum";
import DomainError from "./DomainError";
import GettableError from "./GettableError";

export class PortfolioAlreadyExists extends GettableError implements DomainError {
    constructor() {
        super();
        this.message = "This Portfolio already exists";
        this.statusCode = HttpStatus.BAD_REQUEST;
    }
    message: string;
    statusCode?: number;
}

export class IdIsMissing extends GettableError implements DomainError {
    constructor() {
        super();
        this.message = "Id field is mandatory";
        this.statusCode = HttpStatus.BAD_REQUEST;
    }
    message: string;
    statusCode?: number;
}

export class InvalidFields extends GettableError implements DomainError {
    constructor() {
        super();
        this.message = "Id field is mandatory";
        this.statusCode = HttpStatus.BAD_REQUEST;
    }
    message: string;
    statusCode?: number;
}
