export enum ERROR_TYPE {
    PORTFOLIOALREADYEXISTS = 'PortfolioAlreadyExists',
    IDISMISSING = 'IdIsMissing',
    PORTFOLIONOTFOUND = 'PortfolioNotFound',
    INVALIDFIELDS = 'InvalidFields'
}