import { HttpStatus } from "@nestjs/common";
import DomainError from "./DomainError";
import GettableError from "./GettableError";

export default class PortfolioNotFound extends GettableError implements DomainError {
    constructor() {
        super();
        this.message = "No Portfolio found";
        this.statusCode = HttpStatus.NOT_FOUND;
    }
    message: string;
    statusCode?: number;
}

