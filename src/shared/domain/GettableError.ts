import DomainError from "./DomainError";

export default class GettableError implements DomainError {
    message: string;
    statusCode?: number;

    toObject() {
        const obj = { message: this.message, statusCode: this.statusCode };
        console.log(JSON.stringify(obj)) // Log
        return obj;
    }
}
