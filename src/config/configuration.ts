export default () => ({
    CONSUMER_KEY: process.env.CONSUMER_KEY,
    CONSUMER_SECRET: process.env.CONSUMER_SECRET,
    TOKEN_KEY: process.env.TOKEN_KEY,
    TOKEN_SECRET: process.env.TOKEN_SECRET,
    TWITTER_API: process.env.TWITTER_API,
    AWS_ACCESS_KEY_ID_USER: process.env.AWS_ACCESS_KEY_ID_USER,
    AWS_SECRET_ACCESS_KEY_USER: process.env.AWS_SECRET_ACCESS_KEY_USER,
    AWS_REGION_USER: process.env.AWS_REGION_USER,
});
